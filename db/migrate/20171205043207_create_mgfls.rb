class CreateMgfls < ActiveRecord::Migration[5.1]
  def change
    create_table :mgfls do |t|
      t.string :mgfl_id
      t.string :mgfl_uid
      t.datetime :rec_date
      t.string :fname
      t.string :mname
      t.string :lname
      t.string :email
      t.string :mobile
      t.string :flcard
      t.datetime :synchour_date
      t.datetime :syncweek_date
      t.string :MgMaxDiscountProcent
      t.string :MgBonusCount
      t.string :FLCardNo
      t.string :FLCardStatus
      t.decimal :FlDiscountProcent
      t.decimal :FlBonusCount
      t.decimal :MgMedBonusPer1Rub
      t.decimal :MgLabBonusPer1Rub
      t.decimal :SavingsRub
      t.string :descr
      
      t.timestamps
    end
  end
end
