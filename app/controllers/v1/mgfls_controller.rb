class V1::MgflsController < ApplicationController
    def index
        @mgfls = Mgfl.all

        render json: @mgfls, status: :ok
    end

    # Create Mgfl and return data
    def create
        @mgfl = Mgfl.new(mgfl_params)

        #@mgfl[:mgfl_id] = ""
        uid = 00001000 + @mgfl[:id]
        @mgfl[:mgfl_uid] = "MGFL" + uid.to_s

        @mgfl.save
        render json: @mgfl, status: :created
    end

    # Show Mgfl info
    def show
        @mgfl = Mgfl.find(params[:id])
        render json: @mgfl, status: :ok
    end


    private
    # Required parametres for Mgfl-code generation
    def mgfl_params
        params.require(:mgfl).permit(:lname, :fname, :mname, :email, :mobile)
    end

    def mgfl_calc(mid)
        mgflid = "MGFL" + "%09d" % (mid * 3 + rand(3) + 10000)
    end
end
